# About Spinwheel Tutorial Frontend

Welcome to your Spinwheel Tutorial starter code! This repository is designed to showcase a minimalistic implementation of Spinwheel's Drop-In Modules and model how you could quickly be up and running using the three largest Spinwheel functions: connecting a loan using the Connect DIM, displaying a customer's loan details with the Loan Pal DIM, and finally enabling a customer to make a payment on their loans using the Precision Pay DIM. The frontend is build with React and Material-UI (to minimize the styling needs).

## A Small Note on Usage

When it comes to supplying the DIM token that makes these modules run, that fetch requires a server-to-server call, which means you have two options:

1. The first option is to send a Postman request every time you render a page (since you need a fresh DIM token for each render). We highly recommend against this option(!), but if you insist, we put together a Postman collection [here](https://bitbucket.org/spinwheel/swsl-dim-token-request/src/master/).
2. The second option is getting a copy of the backend starter we created, making a couple **small** tweaks, then sit back and enjoy the benefits of having that work done for you. We're all about giving you choices, so its up to you, but we're biased and think you should take our word for it. That backend code you can grab [here](https://bitbucket.org/spinwheel/swsl-tutorial-backend/src/master/)

# Getting Started

To begin, open a terminal window and clone this repository onto your local machine using the command:

```
git clone git@bitbucket.org:spinwheel/swsl-tutorial-frontend.git
```

Once cloned, cd in the directory like so:

```
cd swsl-tutorial-frontend
```

Now you'll want to install the node modules for the frontend using the npm installation command:
```
npm install
```

Finally, start the frontend with the command:
```
npm start
```

If you've followed instructions, your browser should automatically load the application and you should see the standard React success message in the terminal informing you that the application is live on port 3000.
# Using the Application

This application has everything you need out-of-the-box to follow along in with the Quickstart Tutorial. We've already set up the components with routing, added a few elements here and there with some styling, even dropped a few blank useEffect hooks in places where you'll need them. You should find everything you need within the tutorial on our website, but if you ever have any questions we're always around to help out. Happy coding!