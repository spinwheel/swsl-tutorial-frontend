import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Connect } from './components/Connect'
import { Dashboard } from './components/Dashboard'
import { LoanPal } from './components/LoanPal'
import { PrecisionPay } from './components/PrecisionPay'
import './styles.css'

export const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Dashboard}/>
        <Route exact path="/connect" component={Connect}/>
        <Route exact path="/loanPal" component={LoanPal}/>
        <Route exact path="/precisionPay" component={PrecisionPay}/>
      </Switch>
    </Router>
  )
}
