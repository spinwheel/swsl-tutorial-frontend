import React, { useEffect } from 'react';
import { Button, makeStyles } from '@material-ui/core';
import { useHistory } from 'react-router';

const useStyles = makeStyles(theme => ({
  button: {
    marginTop: theme.spacing(5),
    padding: '10px 36px',
    fontSize: 18,
    fontWeight: 700,
  },
}));

export const LoanPal = () => {
  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
  }, [])

  return (
    <>
      <div style={{ height: '85%', width: '100%' }} id="dropin-container"></div>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignContent: 'center',
          justifyContent: 'center',
        }}
      >
        <Button
          variant="contained"
          size="large"
          alignSelf="center"
          color="primary"
          className={classes.button}
          onClick={() => history.push('/precisionPay')}
        >
          Make a Payment
        </Button>
      </div>
    </>
  );
};
